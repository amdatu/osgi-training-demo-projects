package net.luminis.weatherstation.sensor.api;

public interface Sensor {

    String TYPE = "sensor.type";
    String getName();
    String getUnit();
    double getValue();

}
