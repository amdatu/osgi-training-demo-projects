package net.luminis.weatherstation.sensor.temperature;

import net.luminis.weatherstation.sensor.api.Sensor;

import java.util.Random;

public class TemperatureSensor implements Sensor {
    @Override
    public String getName() {
        return "temperature";
    }

    @Override
    public String getUnit() {
        return "c";
    }

    @Override
    public double getValue() {
        return new Random().nextInt(40);
    }
}
