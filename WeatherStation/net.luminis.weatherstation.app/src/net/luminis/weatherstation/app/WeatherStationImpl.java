package net.luminis.weatherstation.app;

import net.luminis.weatherstation.api.WeatherStation;
import net.luminis.weatherstation.sensor.api.Sensor;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

public class WeatherStationImpl implements WeatherStation {

    private BundleContext bundleContext;
    private ServiceTracker<Sensor, Sensor> tempSensorTracker;
    private ServiceTracker<Sensor, Sensor> pressureSensorTracker;
    private ServiceTracker<Sensor, Sensor> humiditySensorTracker;

    public WeatherStationImpl(BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }

    void start() throws Exception {
        tempSensorTracker = new ServiceTracker<>(bundleContext,
                bundleContext.createFilter(String.format("(&(objectClass=*Sensor)(%s=temperature))", Sensor.TYPE)), null);
        tempSensorTracker.open();
        pressureSensorTracker = new ServiceTracker<>(bundleContext,
                bundleContext.createFilter(String.format("(&(objectClass=*Sensor)(%s=pressure))", Sensor.TYPE)), null);
        pressureSensorTracker.open();
        humiditySensorTracker = new ServiceTracker<>(bundleContext,
                bundleContext.createFilter(String.format("(&(objectClass=*Sensor)(%s=humidity))", Sensor.TYPE)), null);
        humiditySensorTracker.open();
    }

    void stop() {
        tempSensorTracker.close();
        pressureSensorTracker.close();
        humiditySensorTracker.close();
    }

    @Override
    public Double getTemperature() {
        Sensor sensor = tempSensorTracker.getService();
        if (sensor != null) {
            return sensor.getValue();
        }
        return null;
    }

    @Override
    public Double getHumidity() {
        Sensor sensor = humiditySensorTracker.getService();
        if (sensor != null) {
            return sensor.getValue();
        }
        return null;
    }

    @Override
    public Double getPressure() {
        Sensor sensor = pressureSensorTracker.getService();
        if (sensor != null) {
            return sensor.getValue();
        }
        return null;
    }
}
