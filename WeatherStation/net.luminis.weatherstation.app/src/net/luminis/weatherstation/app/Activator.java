package net.luminis.weatherstation.app;

import net.luminis.weatherstation.api.WeatherStation;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

    private Thread looper;
    private WeatherStationImpl weatherStation;

    @Override
    public void start(BundleContext context) throws Exception {
        weatherStation = new WeatherStationImpl(context);
        weatherStation.start();

        looper = new Thread(() -> {
            while(true) {
                System.out.println("Weather report:");
                System.out.println("----");
                System.out.println(String.format("Temperature: %f", weatherStation.getTemperature()));
                System.out.println(String.format("Pressure: %f", weatherStation.getPressure()));
                System.out.println(String.format("Humidity: %f", weatherStation.getHumidity()));
                System.out.println();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        });
        looper.start();
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        looper.interrupt();
        weatherStation.stop();
    }
}
