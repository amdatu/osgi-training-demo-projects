package net.luminis.weatherstation.sensor.humidity;

import net.luminis.weatherstation.sensor.api.Sensor;

import java.util.Random;

public class HumiditySensor implements Sensor {
    @Override
    public String getName() {
        return "humidity";
    }

    @Override
    public String getUnit() {
        return "pct";
    }

    @Override
    public double getValue() {
        return new Random().nextInt(100);
    }
}
