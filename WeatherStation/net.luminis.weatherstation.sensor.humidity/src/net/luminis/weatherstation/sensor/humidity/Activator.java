package net.luminis.weatherstation.sensor.humidity;

import net.luminis.weatherstation.sensor.api.Sensor;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import java.util.Dictionary;
import java.util.Hashtable;

public class Activator implements BundleActivator {

    private ServiceRegistration<Sensor> serviceRegistration;

    @Override
    public void start(BundleContext context) throws Exception {
        Sensor humiditySensor = new HumiditySensor();
        Dictionary<String, Object> props = new Hashtable<>();
        props.put(Sensor.TYPE, "humidity");
        serviceRegistration = context.registerService(Sensor.class, humiditySensor, props);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        serviceRegistration.unregister();
    }
}
