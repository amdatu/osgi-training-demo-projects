package net.luminis.weatherstation.api;

public interface WeatherStation {

    Double getTemperature();
    Double getHumidity();
    Double getPressure();

}
