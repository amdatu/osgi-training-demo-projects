package net.luminis.weatherstation.sensor.pressure;

import net.luminis.weatherstation.sensor.api.Sensor;

import java.util.Random;

public class PressureSensor implements Sensor {
    @Override
    public String getName() {
        return "pressure";
    }

    @Override
    public String getUnit() {
        return "hpa";
    }

    @Override
    public double getValue() {
        return new Random().nextInt(2000);
    }
}
